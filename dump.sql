-- MySQL dump 10.13  Distrib 5.5.35, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: city17
-- ------------------------------------------------------
-- Server version	5.5.35-0ubuntu0.12.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `channels`
--

DROP TABLE IF EXISTS `channels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channels` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `channels`
--

LOCK TABLES `channels` WRITE;
/*!40000 ALTER TABLE `channels` DISABLE KEYS */;
INSERT INTO `channels` VALUES (1,'main',1);
/*!40000 ALTER TABLE `channels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logs` (
  `user_id` int(11) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logs`
--

LOCK TABLES `logs` WRITE;
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;
INSERT INTO `logs` VALUES (1,'1','2014-02-07 22:02:05'),(1,'1','2014-02-07 22:02:18'),(1,'1','2014-02-07 22:02:41'),(1,NULL,'2014-02-07 22:03:45'),(1,NULL,'2014-02-07 22:03:56'),(1,'1','2014-02-07 22:05:00'),(1,'1','2014-02-08 22:02:36'),(1,'1','2014-02-08 22:02:38'),(1,'1','2014-02-08 22:04:37'),(1,'1','2014-02-08 22:09:41'),(6,'1','2014-02-08 22:15:24'),(6,'1','2014-02-08 22:17:18'),(6,'1','2014-02-08 22:19:17');
/*!40000 ALTER TABLE `logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media`
--

DROP TABLE IF EXISTS `media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `type` int(1) NOT NULL,
  `artist` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` text,
  `source` varchar(45) DEFAULT NULL,
  `source_id` varchar(45) DEFAULT NULL,
  `duration` int(5) DEFAULT NULL,
  `group` varchar(45) DEFAULT NULL,
  `thumb` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=472 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media`
--

LOCK TABLES `media` WRITE;
/*!40000 ALTER TABLE `media` DISABLE KEYS */;
INSERT INTO `media` VALUES (35,1,'Moby','Why Does My Heart Feel So Bad','http://s4-4.pleer.com/0257a9bee637579b7123cffc9046a96aadeefa0263e1d2b2fb0f1aafa35dc350b953d55b6473f1875f4bc69d681bbc491a9a4d7c7de00f46c5628a1cb951d9574d75b02b/8ff22bed37.mp3','pleer','5155Bhlq',264,'1260720ZiAR',NULL),(36,1,'GTA 5','New Theme Song','http://s8-4.pleer.com/0757a8beb231579b7920cbeacf06b43d90b2a23c178dd2d0b35b1aa5a94f90249c4f98762141ec9d182df888685e8a5414bb43750095125a93/33e191c692.mp3','pleer','5961820BTuk',154,'1260720ZiAR',NULL),(37,1,'Nina Simone','Feeling Good','http://s5-4.pleer.com/5157fdbee260579a762fc3e0ca02b43d90b2a2352aa293c5c01f5784a25d905dd46c90766d7bed945f4acc97601bb45656bd496a73d81047fd299542e9/40769549a2.mp3','pleer','4699274l6zt',175,'1260720ZiAR',NULL),(38,1,'Dido','White flag','http://s6-3.pleer.com/5357acbeb630579c7523c8e5ce18f720d3bbdc1227a3d2c8b3215282b85d9016984b92335a62ef961a7f8d9b6b56b20857a81f/67e473d5f7.mp3','pleer','255273yfnr',242,'1260720ZiAR',NULL),(39,1,'Dido','Here with me','http://s7-4.pleer.com/5457fabeb066579c7523c8e3cf18f720d3bbdc1227a3d2c8b33e5f99a918c7198042d57e6432d8831368c68a2a58804b67f641686e/a75d84f2bd.mp3','pleer','255212ViRS',254,'1260720ZiAR',NULL),(40,1,'Bonobo','Days To Come (Feat. Bajka)','http://s6-4.pleer.com/0357febee134579f7022c2e0c401b43d90b2a2392ca29d87fc5617cb8859c903d47e9a33427dee965f25e59d654fc10678b946733c925f71d06b9d57a810d7650e45ee750b7e/ecf2794ba8.mp3','pleer','84745JwBX',229,'1260720ZiAR',NULL),(41,1,'Bonobo','Between The Lines (Feat. Bajka)','http://s4-2.pleer.com/0157f4bee165579f7022c2e0c400b43d90b2a2392ca29d87fc5617cb8e5dc407914f9b33557ae6d33364cd9d771bc7605fb958367df91e40cb66d112814ed86f066aee7b1420220e5ff8e7/89d6855475.mp3','pleer','84744VicW',276,'1260720ZiAR',NULL),(42,1,'The Crystal Method','Jaded','http://s1-3.pleer.com/0057f5bee23757997522c3eacf06b43d90b2a22f2ba9d2a6e10f499fad54903d915e9d7c6532aed3356cc79d601bb45656bd496a73d81047fd299542e9/4b45d7a8eb.mp3','pleer','7549820FkFO',425,'1260720ZiAR',NULL),(43,1,'Thievery Corporation','All That We Perceive','http://s6-2.pleer.com/0557f8bee462579a7422cde0c903b43d90b2a22f2ba59793f60443cb8f57c2009b589467687dedd3522de294681bbb4e5bac0c4f389b2f4fd2649d5bac5b94511374a57d09631c4f5fd5fa5b9053/4a2ac9f789.mp3','pleer','4447245V0lK',226,'1260720ZiAR',NULL),(44,1,'Thievery Corporation','Until The Morning','http://s4-2.pleer.com/5657a8beb53357977526c3e4cf18f720d3bbcc132aa98480e10f1aa8a34ac01f864b817a6e7ca3de5f58cd8c6d57cf7252bd0c5532c91143ce60d869aa52d16f1136a3771610514d42bb/557bcbbb2b.mp3','pleer','9509626aO7',237,'1260720ZiAR',NULL),(45,1,'The Crystal Method','Bad Stone','http://s5-2.pleer.com/0157fbbeb862579f7227cde7ce18f720d3bbcc1326ecb197ea054e8aa018fd1580429a77213fa3b11e6983ab705481431a835c7438de0d04c368956ff453c439/9cbbbdc8ac.mp3','pleer','121753sDME',309,'1260720ZiAR',NULL),(46,1,'DJ Shadow','Midnight In A Perfect World','http://s7-3.pleer.com/0457f9bee133579f792fc9e5c801b43d90b2a23f09eca18df212559cec15903d9d4e9b7a667af7d3366383b9246b8a545cbd4f6c7dec1058cc63d869aa52d16f1136a3771610514d42bb/26fef5ba33.mp3','pleer','1993757tjXq',300,'1260720ZiAR',NULL),(47,1,'Vendetta','Ð’ Ð³Ð¾Ñ€Ð»Ðµ ÑÑƒÑˆÐ¸Ñ‚ feat. The Chemodan &','http://s4-2.pleer.com/0757f5beb130579a7926ccebcb05b43d90b2a22d26a29680e7025bcbe11860e2d4fa46c3bfc30323c4dd16d8d5ba3ea5eb50fca08c395f4cc5668c1cfa6adc6f435ba87d16221b415ca8f216a212db463055b09e2dadf935522b01b60226fac0923ea06e1d2bcbad/fdfe2774a7.mp3','pleer','4906963sluQ',316,'1260720ZiAR',NULL),(48,1,'Moby','Extreme Ways (ÐŸÑ€ÐµÐ²Ð¾ÑÑ…Ð¾Ð´ÑÑ‚Ð²Ð¾ Ð‘Ð¾','http://s5-2.pleer.com/0757fabeb033579a732ecae7c400b43d90b2a2362cae8bc5be567f93b84ad51d910aa2727861a3dbaf927278d48e3f94ea66fd998c3eaf9470b329b30bbc64b8b3a6e0c8ea9dc1f1b25869e65049927e2b19979a25f6f13b1f2d5fb71770/18d556fda6.mp3','pleer','43805965HjN',237,'1260720ZiAR',NULL),(49,1,'ÐžÐ´Ð½Ð¾ÐÐ¾ feat SunSay','08. Ð¡Ð²Ð¾Ð±Ð¾Ð´Ð½Ñ‹Ð¹ Ð˜Ð»Ð¸ ÐœÑ‘Ñ€Ñ‚Ð²Ñ‹Ð¹','error','pleer','580908289a2',253,'1260720ZiAR',NULL),(50,1,'The Chemodan Ft. Brick Bazuka','Ð—Ð°Ð¿Ð°Ñ… Ð£Ñ€Ð±Ð°Ð½Ð°','error','pleer','5119557sI0k',193,'1260720ZiAR',NULL),(51,1,'Moby','Why Does My Heart Feel So Bad','error','pleer','5155Bhlq',264,'1260720ZiAR',NULL),(52,1,'GTA 5','New Theme Song','error','pleer','5961820BTuk',154,'1260720ZiAR',NULL),(53,1,'Nina Simone','Feeling Good','error','pleer','4699274l6zt',175,'1260720ZiAR',NULL),(54,1,'Dido','White flag','error','pleer','255273yfnr',242,'1260720ZiAR',NULL),(55,1,'Dido','Here with me','error','pleer','255212ViRS',254,'1260720ZiAR',NULL),(56,1,'Bonobo','Days To Come (Feat. Bajka)','error','pleer','84745JwBX',229,'1260720ZiAR',NULL),(57,1,'Bonobo','Between The Lines (Feat. Bajka)','error','pleer','84744VicW',276,'1260720ZiAR',NULL),(58,1,'The Crystal Method','Jaded','error','pleer','7549820FkFO',425,'1260720ZiAR',NULL),(59,1,'Thievery Corporation','All That We Perceive','error','pleer','4447245V0lK',226,'1260720ZiAR',NULL),(60,1,'Thievery Corporation','Until The Morning','error','pleer','9509626aO7',237,'1260720ZiAR',NULL),(61,1,'The Crystal Method','Bad Stone','error','pleer','121753sDME',309,'1260720ZiAR',NULL),(62,1,'DJ Shadow','Midnight In A Perfect World','error','pleer','1993757tjXq',300,'1260720ZiAR',NULL),(63,1,'Vendetta','Ð’ Ð³Ð¾Ñ€Ð»Ðµ ÑÑƒÑˆÐ¸Ñ‚ feat. The Chemodan &','error','pleer','4906963sluQ',316,'1260720ZiAR',NULL),(64,1,'Moby','Extreme Ways (ÐŸÑ€ÐµÐ²Ð¾ÑÑ…Ð¾Ð´ÑÑ‚Ð²Ð¾ Ð‘Ð¾','error','pleer','43805965HjN',237,'1260720ZiAR',NULL),(65,1,'ÐžÐ´Ð½Ð¾ÐÐ¾ feat SunSay','08. Ð¡Ð²Ð¾Ð±Ð¾Ð´Ð½Ñ‹Ð¹ Ð˜Ð»Ð¸ ÐœÑ‘Ñ€Ñ‚Ð²Ñ‹Ð¹','error','pleer','580908289a2',253,'1260720ZiAR',NULL),(66,1,'The Chemodan Ft. Brick Bazuka','Ð—Ð°Ð¿Ð°Ñ… Ð£Ñ€Ð±Ð°Ð½Ð°','error','pleer','5119557sI0k',193,'1260720ZiAR',NULL),(412,2,'','Nyjah Huston DC Commercial','http://vimeo.com/67624434','vimeo','67624434',0,NULL,'http://b.vimeocdn.com/ts/439/647/439647095_100.jpg'),(413,2,'','The Mission','http://vimeo.com/64841488','vimeo','64841488',0,NULL,'http://b.vimeocdn.com/ts/439/654/439654583_100.jpg'),(414,2,'','Red Bull: Terry Adams BMX \"retrospective\"','http://vimeo.com/62182869','vimeo','62182869',0,NULL,'http://b.vimeocdn.com/ts/431/984/431984457_100.jpg'),(415,2,'','Red Bull \"Perspective\"','http://vimeo.com/60119125','vimeo','60119125',0,NULL,'http://b.vimeocdn.com/ts/418/877/418877383_100.jpg'),(416,2,'','Redbull: Anthony Davis','http://vimeo.com/60115969','vimeo','60115969',0,NULL,'http://b.vimeocdn.com/ts/418/875/418875339_100.jpg'),(417,2,'','National University 3','http://vimeo.com/57280024','vimeo','57280024',0,NULL,'http://b.vimeocdn.com/ts/396/954/396954024_100.jpg'),(418,2,'','National University 2','http://vimeo.com/57279611','vimeo','57279611',0,NULL,'http://b.vimeocdn.com/ts/396/950/396950736_100.jpg'),(419,2,'','National University 1','http://vimeo.com/57278993','vimeo','57278993',0,NULL,'http://b.vimeocdn.com/ts/396/946/396946070_100.jpg'),(420,2,'','Keep Going','http://vimeo.com/57159188','vimeo','57159188',0,NULL,'http://b.vimeocdn.com/ts/396/602/396602601_100.jpg'),(421,2,'','Keep Going','http://vimeo.com/56340850','vimeo','56340850',0,NULL,'http://b.vimeocdn.com/ts/452/778/452778455_100.jpg'),(422,2,'','Darkest Hour \"Survivors\"','http://vimeo.com/53191579','vimeo','53191579',0,NULL,'http://b.vimeocdn.com/ts/367/255/367255287_100.jpg'),(423,2,'','Quicksilver Quik Release Master','http://vimeo.com/53118813','vimeo','53118813',0,NULL,'http://b.vimeocdn.com/ts/367/083/367083286_100.jpg'),(424,2,'','QUIK','http://vimeo.com/47819123','vimeo','47819123',0,NULL,'http://b.vimeocdn.com/ts/339/393/339393576_100.jpg'),(425,2,'','Ford Focus ST Sessions Teaser','http://vimeo.com/46112423','vimeo','46112423',0,NULL,'http://b.vimeocdn.com/ts/320/977/320977889_100.jpg'),(426,2,'','QUIK','http://vimeo.com/46058031','vimeo','46058031',0,NULL,'http://b.vimeocdn.com/ts/339/411/339411306_100.jpg'),(427,2,'','Behind the Curtain | Frank','http://vimeo.com/42247713','vimeo','42247713',0,NULL,'http://b.vimeocdn.com/ts/293/045/293045087_100.jpg'),(428,2,'','ArcLight Intro - Behind the Curtain series','http://vimeo.com/42247529','vimeo','42247529',0,NULL,'http://b.vimeocdn.com/ts/293/039/293039679_100.jpg'),(429,2,'','UNBELEAFABLE','http://vimeo.com/41340506','vimeo','41340506',0,NULL,'http://b.vimeocdn.com/ts/286/622/286622236_100.jpg'),(430,2,'','LAST LIKE DEEP \"Kollisionskurs\"','http://vimeo.com/38804556','vimeo','38804556',0,NULL,'http://b.vimeocdn.com/ts/267/332/267332158_100.jpg'),(431,2,'','The Darkest Hour \"Surviviors\"','http://vimeo.com/38696638','vimeo','38696638',0,NULL,'http://b.vimeocdn.com/ts/266/645/266645177_100.jpg'),(432,2,'','The Dodos','http://vimeo.com/28508431','vimeo','28508431',0,NULL,'http://b.vimeocdn.com/ts/359/374/359374780_100.jpg'),(433,2,'','Unbeleafable: A Girl Skateboards 3D Film','http://vimeo.com/26252591','vimeo','26252591',0,NULL,'http://b.vimeocdn.com/ts/175/803/175803695_100.jpg'),(434,2,'','Eminem promo :60','http://vimeo.com/26146587','vimeo','26146587',0,NULL,'http://b.vimeocdn.com/ts/286/632/286632501_100.jpg'),(435,2,'','The Dodos | Companions','http://vimeo.com/25223433','vimeo','25223433',0,NULL,'http://b.vimeocdn.com/ts/165/958/165958811_100.jpg'),(436,2,'','EMINEM \"Bad meets Evil\" promo','http://vimeo.com/24903904','vimeo','24903904',0,NULL,'http://b.vimeocdn.com/ts/163/636/163636630_100.jpg'),(437,2,'','Check 1,2 Part 3','http://vimeo.com/23764999','vimeo','23764999',0,NULL,'http://b.vimeocdn.com/ts/155/143/155143774_100.jpg'),(438,2,'','Untitled','http://vimeo.com/23764990','vimeo','23764990',0,NULL,'http://b.vimeocdn.com/ts/155/177/155177273_100.jpg'),(439,2,'','Untitled','http://vimeo.com/23764964','vimeo','23764964',0,NULL,'http://b.vimeocdn.com/ts/155/162/155162581_100.jpg'),(440,2,'','Life By Design','http://vimeo.com/23611215','vimeo','23611215',0,NULL,'http://b.vimeocdn.com/ts/153/897/153897781_100.jpg'),(441,2,'','The Dodos \"Companions\"','http://vimeo.com/22958631','vimeo','22958631',0,NULL,'http://b.vimeocdn.com/ts/286/626/286626598_100.jpg'),(442,2,'','Altoids \"Curiously Strong Awards\"','http://vimeo.com/22415297','vimeo','22415297',0,NULL,'http://b.vimeocdn.com/ts/162/400/162400274_100.jpg'),(443,2,'','\"Mongonex\"','http://vimeo.com/20483444','vimeo','20483444',0,NULL,'http://b.vimeocdn.com/ts/130/915/130915816_100.jpg'),(444,2,'','\"Dextra Large\" fake infomercial','http://vimeo.com/20482940','vimeo','20482940',0,NULL,'http://b.vimeocdn.com/ts/130/906/130906976_100.jpg'),(445,2,'','CONTINENTAL TIRE','http://vimeo.com/19954150','vimeo','19954150',0,NULL,'http://b.vimeocdn.com/ts/126/986/126986493_100.jpg'),(446,2,'','ATOMIC TOM \"DON\'T YOU WANT ME\"','http://vimeo.com/19856705','vimeo','19856705',0,NULL,'http://b.vimeocdn.com/ts/286/630/286630182_100.jpg'),(447,2,'','Lakairomania','http://vimeo.com/18720981','vimeo','18720981',0,NULL,'http://b.vimeocdn.com/ts/117/964/117964062_100.jpg'),(448,2,'','DVS Presents | Licky Sticky Fun Grip','http://vimeo.com/17699714','vimeo','17699714',0,NULL,'http://b.vimeocdn.com/ts/110/381/110381166_100.jpg'),(449,2,'','DVS Presents | Decks 4 Diamonds','http://vimeo.com/17165831','vimeo','17165831',0,NULL,'http://b.vimeocdn.com/ts/106/268/106268075_100.jpg'),(450,2,'','DVS Presents | DeXtra Large','http://vimeo.com/16478419','vimeo','16478419',0,NULL,'http://b.vimeocdn.com/ts/102/117/102117991_100.jpg'),(451,2,'','DVS Presents | Legal A.B.D.','http://vimeo.com/16298911','vimeo','16298911',0,NULL,'http://b.vimeocdn.com/ts/995/805/99580520_100.jpg'),(452,2,'','\"Wood\" DVS 2009 Skate and Create','http://vimeo.com/15301622','vimeo','15301622',0,NULL,'http://b.vimeocdn.com/ts/919/446/91944645_100.jpg'),(453,2,'','RED STRIPE','http://vimeo.com/15292228','vimeo','15292228',0,NULL,'http://b.vimeocdn.com/ts/919/455/91945569_100.jpg'),(454,2,'','\"LAKAIROMANIA\"','http://vimeo.com/14681630','vimeo','14681630',0,NULL,'http://b.vimeocdn.com/ts/286/622/286622904_100.jpg'),(455,2,'','Fuel TV \"Mobius Tricks\"','http://vimeo.com/14515349','vimeo','14515349',0,NULL,'http://b.vimeocdn.com/ts/900/677/90067729_100.jpg'),(456,2,'','FUEL TV - Mobius Tricks (Directors cut)','http://vimeo.com/11956628','vimeo','11956628',0,NULL,'http://b.vimeocdn.com/ts/913/705/91370580_100.jpg'),(457,2,'','Pacsun commercial','http://vimeo.com/11392086','vimeo','11392086',0,NULL,'http://b.vimeocdn.com/ts/624/234/62423494_100.jpg'),(458,2,'','DVS \"Use #1\" with Torey Pudwill','http://vimeo.com/11392001','vimeo','11392001',0,NULL,'http://b.vimeocdn.com/ts/893/752/89375262_100.jpg'),(459,2,'','DVS SHINE Commercials','http://vimeo.com/9589516','vimeo','9589516',0,NULL,'http://b.vimeocdn.com/ts/479/270/47927093_100.jpg'),(460,2,'','DVS Mix \'N Match commercial','http://vimeo.com/8222896','vimeo','8222896',0,NULL,'http://b.vimeocdn.com/ts/377/626/37762685_100.jpg'),(461,2,'','DVS \"Mix N Match\"','http://vimeo.com/8125539','vimeo','8125539',0,NULL,'http://b.vimeocdn.com/ts/900/711/90071115_100.jpg'),(462,2,'','Metro Station \"Kelsey\"','http://vimeo.com/7032613','vimeo','7032613',0,NULL,'http://b.vimeocdn.com/ts/289/011/28901181_100.jpg'),(463,2,'','The Corner','http://vimeo.com/6707180','vimeo','6707180',0,NULL,'http://b.vimeocdn.com/ts/160/026/160026501_100.jpg'),(464,2,'','Lenka â€œTrouble is a Friendâ€','http://vimeo.com/6692980','vimeo','6692980',0,NULL,'http://b.vimeocdn.com/ts/268/777/268777918_100.jpg'),(465,2,'','The White Tie Affair \"Take it Home\"','http://vimeo.com/6692917','vimeo','6692917',0,NULL,'http://b.vimeocdn.com/ts/262/349/26234986_100.jpg'),(466,2,'','Great Lake Swimmers \"Pulling on a Line\"','http://vimeo.com/6493592','vimeo','6493592',0,NULL,'http://b.vimeocdn.com/ts/917/985/91798546_100.jpg'),(467,2,'','Sweethead \"The Great Disruptors\"','http://vimeo.com/6493562','vimeo','6493562',0,NULL,'http://b.vimeocdn.com/ts/900/729/90072929_100.jpg'),(468,2,'','Cage the Elephant \"Back against the wall\"','http://vimeo.com/6493293','vimeo','6493293',0,NULL,'http://b.vimeocdn.com/ts/901/356/90135679_100.jpg'),(469,2,'','DVS Skate & Create 2009 Feature','http://vimeo.com/6214768','vimeo','6214768',0,NULL,'http://b.vimeocdn.com/ts/227/390/22739092_100.jpg'),(470,2,'','09 Director of Photography Reel','http://vimeo.com/3725864','vimeo','3725864',0,NULL,'http://b.vimeocdn.com/ts/920/033/92003340_100.jpg'),(471,2,'','DVS Montage','http://vimeo.com/2939061','vimeo','2939061',0,NULL,'http://b.vimeocdn.com/ts/864/851/86485152_100.jpg');
/*!40000 ALTER TABLE `media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `music_to_channel`
--

DROP TABLE IF EXISTS `music_to_channel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `music_to_channel` (
  `channel_id` int(3) NOT NULL,
  `media_id` int(11) NOT NULL,
  `type` varchar(45) DEFAULT NULL,
  `from` int(11) DEFAULT NULL,
  `to` int(11) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  PRIMARY KEY (`channel_id`,`media_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `music_to_channel`
--

LOCK TABLES `music_to_channel` WRITE;
/*!40000 ALTER TABLE `music_to_channel` DISABLE KEYS */;
INSERT INTO `music_to_channel` VALUES (1,36,'1',NULL,NULL,NULL,NULL),(1,37,'1',NULL,NULL,NULL,NULL),(1,38,'1',NULL,NULL,NULL,NULL),(1,39,'1',NULL,NULL,NULL,NULL),(1,41,'1',NULL,NULL,NULL,NULL),(1,46,'1',NULL,NULL,NULL,NULL),(1,50,'1',NULL,NULL,NULL,NULL),(1,51,'1',NULL,NULL,NULL,NULL),(1,54,'1',NULL,NULL,NULL,NULL),(1,60,'1',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `music_to_channel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `password` text,
  `salt` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (6,'denys','sd@sdsd.com','b43e522751036abdb909a31c11ef337939707b028351403ffa68e9b2dff98aec1b0f122058a183ea099af9de7445239bd240da0642abb86f5b741e1faeda4edc','3fd30f59f1c5af9490f476849f27e404c522f2ce2a462fc76e97ce5e1090087e73762be847e2e1f4d0e03076499468a869fa39e61845af20f588ad99edccbd46'),(7,'denys','sd@sdsd.com','7d19bd2151acfda48175f9825701378795584b90235367d6fe58c55502f5fb668be6da78a132840b9756804e160a82a27d6a2f72cafaeca1af33bb3ec7ce9252','5cd35024a563fcff4cec05dfa3cb86e9f39b446af04934dccaf5d103d272f6c769fc90debd74e55a1ef84106d56c2691a68119a5c0caebfcbf2d023aa86a7c87'),(8,'denys2','sd2@sdsd.com','2f9885e960382ad02d0797649dfe8ca4e85e80074a1a0c27cb396e5371eef32d80d207b41b09fdeff40ba718400323da119b97604de28237015ec245e83e6d31','e7a37af292e49086ed855d3e3f266c7e92bb22236b9f79fea604a45a297ab37db70b1be8e1171272eea581d7b7e0e1db2a630c09e4c92b5617dba1193b70a388');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-02-17 22:24:04
