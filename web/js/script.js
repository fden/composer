var controlsVisable = false;
var tickes = 0;
var visibleInterval = 2000;
var visibleIntervalLong = 5000;
var tikesInterval = 200;
var tickesVar = visibleInterval/tikesInterval;
var controlsBar = $('#controls');

hideTimer();
window.setInterval(timer, tikesInterval);

function timer()
{
	tickes++;
}

function hideTimer(miliseconds)
{
	window.setTimeout(hideControls, miliseconds ? miliseconds : visibleInterval-tickes*tikesInterval  );
}

$(document).mousemove(function(event) 
{
	tickes = 0;
	if(!controlsVisable)
	{
		hideTimer();
		controlsBar.fadeIn();
		controlsVisable = true;		
	}
});

controlsBar.mouseenter(function()
{
	tickesVar = visibleIntervalLong/tikesInterval;
}).mouseleave(function() {
	tickesVar = visibleInterval/tikesInterval;
});


function hideControls()
{
	if(tickes>tickesVar)
	{
		controlsBar.fadeOut();
		controlsVisable = false;
	}
	else
	{
		hideTimer();
	}
}

$(function(){
	initPlayer();
	// initVideo();
});

function initPlayer()
{
	$("#player").jPlayer({
		ready: function () {
			$(this).jPlayer("setMedia", {
				mp3: play_url
				// mp3: "{{ urls }}"
			}).jPlayer("play");
		},
		ended: function() {
			playNext();
   		},
		swfPath: "js",
		supplied: "mp3",
		keyEnabled: true
	});
}

function playNext()
{
	$.get('/ajax/getNextSong', function(response){
				// $(iframe).attr('src', 'http://player.vimeo.com/video/'+response+'?api=1&player_id=video');
		$('#player').jPlayer("setMedia", {mp3: response}).jPlayer('play')
	});
}
function initVideo2()
{
	var iframe = $('#video')[0],
	    player = $f(iframe);
	    
	// When the player is ready, add listeners for pause, finish, and playProgress
	player.addEvent('ready', function() {
		console.log('sdsd');
	    // status.text('ready');
	    
	    player.addEvent('pause', onPause);
	    player.addEvent('finish', onFinish);
	    player.addEvent('playProgress', onPlayProgress);
	});

	// Call the API when a button is pressed
	$('button').bind('click', function() {
	    player.api($(this).text().toLowerCase());
	});

	function onPause(id) {
		console.log('sdsd');
	    // status.text('paused');
	}

	function onFinish(id) {
	    // status.text('finished');
	}

	function onPlayProgress(data, id) {
	    // status.text(data.seconds + 's played');
	}
}

function initVideo()
{
	(function(){

                // Listen for the ready event for any vimeo video players on the page
                var vimeoPlayers = document.querySelectorAll('iframe'),
                    player;

                for (var i = 0, length = vimeoPlayers.length; i < length; i++) {
                    player = vimeoPlayers[i];
                    $f(player).addEvent('ready', ready);
                }

                /**
* Utility function for adding an event. Handles the inconsistencies
* between the W3C method for adding events (addEventListener) and
* IE's (attachEvent).
*/
                function addEvent(element, eventName, callback) {
                    if (element.addEventListener) {
                        element.addEventListener(eventName, callback, false);
                    }
                    else {
                        element.attachEvent(eventName, callback, false);
                    }
                }

                /**
* Called once a vimeo player is loaded and ready to receive
* commands. You can add events and make api calls only after this
* function has been called.
*/
                function ready(player_id) {
                    console.log(111);
                    // Keep a reference to Froogaloop for this player
                    var container = document.getElementById(player_id).parentNode.parentNode,
                        froogaloop = $f(player_id);
                        // apiConsole = container.querySelector('.console .output');
                        froogaloop.api('pause');
                }

            });


}