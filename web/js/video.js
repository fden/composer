$(function()
{
	var iframe = $('#video')[0],
    player = $f(iframe);

	player.addEvent('ready', function() {
	    player.api('setVolume', 0);
        player.api('setColor', 'eee');
	    player.addEvent('pause', onPause);
	    player.addEvent('play', onPlay);
	    player.addEvent('finish', onFinish);
	    player.addEvent('playProgress', onPlayProgress);
	    player.api('play');
	});

	function onPause(id) {
		$('#fixed_bottom').show();
		$('#fixed_top').show();
	}

	function onPlay(id) {
		setTimeout('hideBackStage()', 5000);		
	}

	

	function onFinish(id) {
		player.api('unload');
	}

	function onPlayProgress(data, id) {
		if(data.percent >= 0.95)
		{
			$.get('/ajax/getNextVideo', function(response){
				$('#fixed_bottom').show();
				$('#fixed_top').show();
				$(iframe).attr('src', 'http://player.vimeo.com/video/'+response+'?api=1&player_id=video');
			});
		}
		// if(data.percent >= 0.)
		// {
		// 	player.api('unload');
		// }

	}
	
});
function hideBackStage()
	{
		// $('#vimeo_controls').hide();
		$('#fixed_bottom').slideUp();
		$('#fixed_top').slideUp();

	}