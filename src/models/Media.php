<?php

		// use Helpers\Pleer;
namespace Models;

class Media {
	public $app;

	function __construct($app)
	{
		$this->app = $app;
	}

	public static function getForm()
	{
		
	}

	public function getForms()
	{
		
	}

	public function getItems()
	{
		return $this->app->db->fetchAll("SELECT * FROM media");
	}

	public static function savePleerPlaylist($url)
	{
		$pleer = new \Helpers\Pleer();
		$tracks = $pleer->getTracks($url);
	}

	public function getGroupMedia()
	{
		$media = $this->app->db->fetchAll("SELECT * FROM media");
		$channel = $this->getLive();
			
		$media = $this->groupBy($media, 'type');
		$media['live'] = $channel;
		// foreach ($media as $key => $value) {
			// $media[$key] = $this->groupBy($media[$key], 'group');
		// }
		// echo "<pre>";
		// print_r($media);die;
		return $media;
	}

	public function getLive()
	{
		return $this->app->db->createQueryBuilder()
				->select('*')
				->from('music_to_channel', 'c')
				->innerJoin('c', 'media', 'm', 'c.media_id = m.id')
				->where('m.type = 1')
				->andWhere('c.channel_id = ?')
				->setParameter(0, '1')
				->execute()->fetchAll();
	}

	public function saveToChannel($params)
	{
		$q = $this->app->db->insert('music_to_channel', $params);
		if($q)
			return true;
		return false;

	}

	public function groupBy($data, $key_to_group)
	{
		

		foreach ($data as $item) {
		    $key = $item[$key_to_group];
		    // if (!isset($groups[$key])) {
		        $groups[$this->getKey($key, $key_to_group)][]  = $item;
		    // } else {
		    //     $groups[$key_name][] = $item;
		    // }
		}
		return $groups;
	}

	public function getNextVideo()
	{
		$videos = ($this->app->db->fetchAll("SELECT * FROM media WHERE source = 'vimeo'"));
		$key = array_rand($videos);
		return $videos[$key];
	}

	public function getNextSong()
	{

		$medias = $this->getLive();
		$key = array_rand($medias);
		$id = $medias[$key]['source_id'];
		$medias[$key]['url'] = \Helpers\Pleer::getListenLinkFromId($id);

		return $medias[$key];
	}

	public function getKey($key, $key_to_group)
	{
		if($key_to_group == 'type'){
			$groups = array();
			switch ($key) {
				case 1:
					$key_name = 'music';
					break;
				case 2:
					$key_name = 'video';
					break;
				default:
					$key_name = 'error';
					break;
			}
		}
		else
			return $key;
		return $key_name;
	}

}