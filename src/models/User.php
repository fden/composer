<?php

namespace Models;

class User
{


	public static function sec_session_start() {
	    $session_name = 'session';   // Set a custom session name 
	    $secure = false;

	    $httponly = true;

	    // if (ini_set('session.use_only_cookies', 1) === FALSE) {
	    //     die('session.use_only_cookies error');
	    // }

	    $cookieParams = session_get_cookie_params();
	    session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly);

	    session_name($session_name);

	    session_start();
	    session_regenerate_id(); 
	}

	public static function login($mysqli, $email = false, $password = false) {

		if(!$email)
		{
			$email = $_POST['email'];
			$password = $_POST['p'];
		}
	    if ($stmt = $mysqli->prepare("SELECT * FROM users WHERE email = ? LIMIT 1")) {
	        $stmt->bindValue(1, $email); 
	        $stmt->execute();    

	        extract($stmt->fetch());
	        $user_id = $id;
	        $db_password = $password;
	        $password = hash('sha512', $_POST['p'] . $salt);
	        if ($id) {
	            if (self::checkbrute($user_id, $mysqli) == true && false) {
	                return false;
	            } else {
	                if ($db_password == $password) {
	                    $user_browser = $_SERVER['HTTP_USER_AGENT'];

	                    $user_id = preg_replace("/[^0-9]+/", "", $user_id);
	                    $_SESSION['user_id'] = $user_id;

	                    $username = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $username);

	                    $_SESSION['username'] = $username;
	                    $_SESSION['login_string'] = hash('sha512', $password . $user_browser);

	                    return true;
	                } else {
	                    // Password is not correct 
	                    $now = time();
	                    if (!$mysqli->insert("logs", array('user_id' => $user_id, 'type' => 1))) {
	                    	return true;
	                    }

	                    return false;
	                }
	            }
	        } else {
	            // No user exists. 
	            return false;
	        }
	    } else {
	        header("Location: ../error.php?err=Database error: cannot prepare statement");
	        exit();
	    }
	}

	public static function checkbrute($user_id, $mysqli) {
	    $now = time();

	    $valid_attempts = $now - (2 * 60 * 60);

	    if ($stmt = $mysqli->prepare("SELECT time FROM logs WHERE user_id = ? AND time > '$valid_attempts'")) {
	        $stmt->bindValue(1, $user_id);

	        $q = $stmt->execute();

	        if (count($stmt->fetchAll()) > 5) {
	            return true;
	        } else {
	            return false;
	        }
	    } else {
	        die('brute db error');
	    }
	}

	public static function login_check($mysqli) {

	    if (isset($_SESSION['user_id'], $_SESSION['username'], $_SESSION['login_string'])) {
	        $user_id = $_SESSION['user_id'];
	        $login_string = $_SESSION['login_string'];
	        $username = $_SESSION['username'];

	        $user_browser = $_SERVER['HTTP_USER_AGENT'];

	        if ($stmt = $mysqli->prepare("SELECT password 
					      FROM users 
					      WHERE id = ? LIMIT 1")) {
	            $stmt->bindValue(1, $user_id);
	            $stmt->execute();

	            if ($result = $stmt->fetch()) {
	                $password = $result['password'];
	                $login_check = hash('sha512', $password . $user_browser);

	                if ($login_check == $login_string) {
	                    // Loggged in
	                    return true;
	                } else {
	                    // Not logged in 
	                    return false;
	                }
	            } else {
	                // Not logged in 
	                return false;
	            }
	        } else {
	            // Cant not prepare statement
	            return false;
	        }
	    } else {
	        // Not logged in 
	        return false;
	    }
	}

	public static function esc_url($url) {

	    if ('' == $url) {
	        return $url;
	    }

	    $url = preg_replace('|[^a-z0-9-~+_.?#=!&;,/:%@$\|*\'()\\x80-\\xff]|i', '', $url);
	    
	    $strip = array('%0d', '%0a', '%0D', '%0A');
	    $url = (string) $url;
	    
	    $count = 1;
	    while ($count) {
	        $url = str_replace($strip, '', $url, $count);
	    }
	    
	    $url = str_replace(';//', '://', $url);

	    $url = htmlentities($url);
	    
	    $url = str_replace('&amp;', '&#038;', $url);
	    $url = str_replace("'", '&#039;', $url);

	    if ($url[0] !== '/') {
	        return '';
	    } else {
	        return $url;
	    }
	}
	public function register($mysqli)
	{
		// $mysqli = $this->app->db;

		if (isset($_POST['username'], $_POST['email'], $_POST['p'])) {
			$error_msg = '';

		    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
		    $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
		    $email = filter_var($email, FILTER_VALIDATE_EMAIL);
		    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		        $error_msg .= '<p class="error">The email address you entered is not valid</p>';
		    }
		    
		    $password = filter_input(INPUT_POST, 'p', FILTER_SANITIZE_STRING);
		    if (strlen($password) != 128) {
		        $error_msg .= '<p class="error">Invalid password configuration.</p>';
		    }

		    $prep_stmt = "SELECT id FROM users WHERE email = :email LIMIT 1";
		    $stmt = $mysqli->prepare($prep_stmt);
		    
		    if ($stmt) {
		        $stmt->bindValue(':email', $email);
		        $stmt->execute();
		        
		        if ($stmt->fetchAll()) {
		            // A user with this email address already exists
		            $error_msg .= '<p class="error">A user with this email address already exists.</p>';
		        }
		    } else {
		        $error_msg .= '<p class="error">Database error</p>';
		    }

		    if (!empty($error_msg)) {
		        $random_salt = hash('sha512', uniqid(openssl_random_pseudo_bytes(16), TRUE));

		        $password = hash('sha512', $password . $random_salt);

		        if ($insert_stmt = $mysqli->prepare("INSERT INTO users (username, email, password, salt) VALUES (?, ?, ?, ?)")) {
		            $insert_stmt->bindValue(1, $username);
		            $insert_stmt->bindValue(2, $email);
		            $insert_stmt->bindValue(3, $password);
		            $insert_stmt->bindValue(4, $random_salt);

		            if (! $insert_stmt->execute()) {
		                die('error');
		            }
		        }
		        die('ok');
		    }
		}
	}

	public static function logout()
	{
		// sec_session_start();
		$_SESSION = array();
		$params = session_get_cookie_params();
		setcookie(session_name(),'', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
		session_destroy();
	}
}