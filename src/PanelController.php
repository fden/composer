<?php

use Models\Media;
use Helpers\Forms;

class PanelController
{
	public $params = array();

	function __construct($app, $request)
	{
		$this->app = $app;
		$this->request = $request;
		if(isset($request['module']))
		{
			$this->params['module'] = $request['module'];
		}
		$action = $this->request['action'].'Action';
		if(!$this->app->user->login_check($this->app->db))
			$this->$action();
		else
			$this->login();
	}

	private function indexAction()
	{
		$this->params['media'] = $this->app->media->getGroupMedia();
		$this->params['live'] = $this->app->media->getLive();
		$this->render();
	}

	private function moduleAction()
	{
		if(isset($this->request['id']))
			$this->params['form'] = $this->getForm();
		$this->params['action'] = isset($this->request['id']) ? 'edit' : 'list';
		$this->params['items'] = $this->app->media->getItems();
		$this->render();
	}

	private function render()
	{
		echo $this->app->c['twig']->render('layouts/panel.html', $this->params);
		// echo $this->app->c['twig']->render('panel/'.$this->request['view'].'.html', $this->params);
	}

	private function login()
	{
		echo $this->app->c['twig']->render('layouts/panel.html', $this->params);

	}

	private function getForm()
	{
		// $form = new Forms();
		// $result = $form->startForm('', 'post', '', array('class'=>'demoForm', 'onsubmit'=>'return checkBeforeSubmit(this)') ) . PHP_EOL .
  //   	$form->startTag('fieldset') . PHP_EOL .
  //   	$form->startTag('legend') . 'Example Form' . $form->endTag() . PHP_EOL .
  //   	$form->endTag('fieldset') . PHP_EOL .
  //   	$form->endForm();
  //   	return $result;
		return $this->app->kit->createForm($this->params['module']);
	}
}