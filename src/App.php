<?php

use Aura\Router\RouterFactory;

class App
{
	public $c;

	function __construct()
	{
		$this->c = new Pimple();
		$router_factory = new RouterFactory;
		$router = $router_factory->newInstance();

		//---- INIT CONTEINER
		$this->c['db'] = function ($c) {
			$connectionParams = array(
			    'dbname' => 'city17',
			    'user' => 'root',
			    'password' => 'root',
			    'host' => 'localhost',
			    'driver' => 'pdo_mysql',
			);
			$driver = new \Doctrine\DBAL\Driver\PDOMySql\Driver;
			return new \Doctrine\DBAL\Connection($connectionParams, $driver);
		};

		$this->c['twig'] = function ($c) {
			$loader = new Twig_Loader_Filesystem(__DIR__ .'/views');
			return new Twig_Environment($loader);
		};

		$this->c['kit'] = function ($c) {
			return new Kit('src/config/');
		};

		$this->c['user'] = function ($c) {
			return new Models\User();
		};

		$this->c['media'] = function($c){
			return new Models\Media($this);
		};

		//---- INIT ROUTINGS
		$router->add('home', '/')
			->addValues(array(
		        'controller' => 'IndexController',
		        'action'     => 'index',
		        'view'		 =>	'home'
		));

		$router->add('panel', '/panel')
			->addValues(array(
		        'controller' => 'PanelController',
		        'action'     => 'index',
		        'view'     	 => 'panel'
				));

		$router->add('module', '/panel{/module,id}')
			->addValues(array(
		        'controller' => 'PanelController',
		        'action'     => 'module',
		        'view'     	 => 'panel'
				));

		$router->add('user', '/{user_action}')
			->addTokens(array(
				'user_action' => '(register|login|login_check|logout)'
			))
			->addValues(array(
				'controller' => function($params){

					$this->user->$params['user_action']($this->db);
				},
				'run' => true,
		));

		$router->add('ajax', '/ajax/{action}')
			->addValues(array(
				'controller' => 'AjaxController'
		));

		$router->add('editor', '/editor')
			->addValues(array(
				'controller' => function()
				{
					echo $this->twig->render('editor.html');
				},
				'run' => true
		));

		$this->user->sec_session_start();
		$this->getRoutingReselt($router);
	}

	public function __get($name)
	{
		if(isset($this->c[$name]))
			return $this->c[$name];
	}

	private function getRoutingReselt($router)
	{
		//match result
		$path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
		$result = $router->match($path, $_SERVER);

		//without controller class
		if(isset($result->params['run']))
		{
			$params = $result->params;
			$controller = $params["controller"];
			unset($params["controller"]);
			echo $controller($params);
		}
		//with controller class
		elseif(isset($result->values['controller']))
			new $result->values['controller']($this, $result->params);
		//if no result 404
		else
			die("404");
	}
}