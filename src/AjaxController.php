<?php

class AjaxController
{

	function __construct($app, $request)
	{
		$this->app = $app;
		$this->request = $request;
		$action = $this->request['action'];
		$this->$action();
	}
	function saveToChannel()
	{
		echo $this->app->media->saveToChannel($_POST);
	}
	function getNextVideo()
	{
		$videos = ($this->app->db->fetchAll("SELECT * FROM media WHERE source = 'vimeo'"));
		$key = array_rand($videos);
		echo $videos[$key]['source_id'];
	}

	function getNextSong()
	{
		$medias = ($this->app->db->fetchAll("SELECT * FROM media WHERE source = 'pleer'"));
		$key = array_rand($medias);
		$id = $medias[$key]['source_id'];
		echo Helpers\Pleer::getListenLinkFromId($id);
	}

	function parseMedia()
	{
		$url = $_POST['media_url'];
		$urlArray = parse_url($url);

		switch ($urlArray['host']) {
			case 'vimeo.com':
				\Helpers\Vimeo::parseVideos($url, $this->app->db);
				break;
			default:
				echo 0;
				break;
		}

	}

	function getObj(){
		echo file_get_contents('src/config/obj.json');
	}

}