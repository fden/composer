<?php

namespace Helpers;
use Sunra\PhpSimple\HtmlDomParser;

class Pleer {
	
	function __construct()
	{

	}

	public static function getTracks($playlist_id = null)
	{
		$html = HtmlDomParser::file_get_html($playlist_id);
		$dom = HtmlDomParser::str_get_html( $html );

		$tracks = array();

		foreach($html->find('ol#search-results') as $e){
    		foreach($e->find('li') as $li) {
    			$track = array(
    				'type' => 1,
    				'url' => '',
    				'artist' => $li->singer,
    				'name' =>	$li->song,
    				'duration' => $li->duration,
    				'source' => 'pleer',
    				'source_id' => $li->link
    			);
    			$tracks[] = $track;
       		}

   		}

   		foreach ($tracks as $key => $value) {
   			if($url = self::getListenLinkFromId($value['source_id'])){
   				$tracks[$key]['url'] = $url;
   			}
   		}

   		return $tracks;
	}

	public static function getListenLinkFromId($id)
	{
		$url = 'http://pleer.com/site_api/files/get_url';
		$data = array(
			'id' => $id, 
			'action' => 'listen'
		);

		$options = array(
		    'http' => array(
		        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
		        'method'  => 'POST',
		        'content' => http_build_query($data),
		    ),
		);
		$context  = stream_context_create($options);
		$result = file_get_contents($url, false, $context);
		$result = (array)json_decode($result);
		if(isset($result['track_link']))
			return $result['track_link'];
		return 'error';
	}
}