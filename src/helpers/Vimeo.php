<?php
    // Client ID: d77c6f0efb500dc361a3f982839b0a327350f0b5
    // Client secret: 73ea6922445b156506a61e0af644e52e37d412cc
    // OAuth access token: 71ed4a3b97561778ffb1aeadf2c04cca
namespace Helpers;

class Vimeo{

	public static function getAlbumVideos($name)
	{
		$url = "http://vimeo.com/api/v2/album/$name/videos.json";
		$url = "http://vimeo.com/api/v2/group/30665/videos.json";
		$response =  file_get_contents($url);
		$response = json_decode($response);

		$tracks = array();
		foreach ($response as $key => $value) {
			$tracks[] = self::saveVideo((array)$value);
			// echo "<img src={$value['thumbnail_small']}>";
		}
		return $tracks;
	}

	public static function saveVideo($e)
	{
		$data = array(
			'type' => 2,
    		'url' => $e['url'],
    		'artist' => '',
    		'name' =>	$e['title'],
    		'duration' => '',
    		'source' => 'vimeo',
    		'source_id' => $e['id'],
    		'thumb' => $e['thumbnail_small']
		);
		return $data;
	} 

	public static function parseVideos($url, $db)
	{
		$urlArray = parse_url($url);

		$tracks = array();
		if(preg_match('/user\d*/', $urlArray['path'], $matches)){
			for ($i=1; $i<=3; $i++) { 
				$get = "http://vimeo.com/api/v2/{$matches[0]}/all_videos.json?page=$i";

				$t = self::getContent($get, $db);
				if(!$t || count($t) < 20)
					break;
			}
		}

		return $tracks;
	}

	public static function getContent($url, $db = false)
	{
		$response =  file_get_contents($url);
		$response = json_decode($response);

		$tracks = array();
		foreach ($response as $key => $value) {
			$track = self::saveVideo((array)$value);
			if($db)
				$db->insert('media', $track);
			$tracks[] = $track;
		}
		return $tracks;
	}
}