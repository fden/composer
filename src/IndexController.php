<?php

class IndexController
{
	public $params = array();
	function __construct($app, $request)
	{
		$this->app = $app;
		$this->request = $request;
		$action = $this->request['action'].'Action';
		$this->$action();
	}

	private function indexAction()
	{
		$audio = $this->app->media->getNextSong();
		$videos = $this->app->media->getNextVideo();

		$this->params['audio'] = ($audio);
		$this->params['video'] = ($videos);
		// print_r($this->params);die;
		$this->render();
	}

	private function render()
	{
		echo $this->app->c['twig']->render($this->request['view'].'.html', $this->params);
	}
}