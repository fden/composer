<?php
// application.php

require_once 'vendor/autoload.php';
use \Commands\DbCommand;
use \Commands\FormCommand;
use Symfony\Component\Console\Application;

$application = new Application();
$application->add(new DbCommand);
$application->add(new FormCommand);
$application->run();