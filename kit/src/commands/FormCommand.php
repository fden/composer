<?php

namespace Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Component\Yaml\Yaml;

use Creator;

class FormCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('forms')
            ->setDescription('form commands')
            ->addArgument(
                'action',
                InputArgument::REQUIRED,
                'update/make'
            )
            ->addOption(
               'structure',
               null,
               InputOption::VALUE_NONE,
               'Only structure'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $text = '';
        $name = $input->getArgument('action');
        $array = Yaml::parse('src/database.yml');
        switch ($name) {
            case 'make':
                Creator::makeForms($array);
                break;
            case 'render':
                Creator::renderForms($array);
                break;
            default:
                $text = 'undefind command';
                break;
        }

        if ($input->getOption('structure')) {

        }

        $output->writeln($text);
    }
}