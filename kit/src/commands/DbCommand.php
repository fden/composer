<?php

namespace Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Component\Yaml\Yaml;

use Creator;

class DbCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('kit')
            ->setDescription('kit commands')
            ->addArgument(
                'task',
                InputArgument::REQUIRED,
                'install/update/dump/flush'
            )
            ->addArgument(
                'action',
                InputArgument::REQUIRED,
                'install/update/dump/flush'
            )
            ->addArgument(
                'module',
                InputArgument::OPTIONAL,
                'module name'
            );
            // ->addOption(
            //    'structure',
            //    null,
            //    InputOption::VALUE_NONE,
            //    'Only structure'
            // )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $text = '';
        $name = $input->getArgument('action');
        $module = $input->getArguments();
        $creator = new Creator($module);
        die('---');
        $array = Yaml::parse('src/database.yml');
        switch ($name) {
            case 'install':
                // $text = 'install';
                // print_r($array);die;
                Creator::createDb($array);
                break;
            case 'flush':
                Creator::dropDb($array);
                break;
            case 'update':
                Creator::updateDb($array);
                break;
            case 'forms':
                Creator::createForms($array);
                break;
            default:
                $text = 'undefind command';
                break;
        }

        if ($input->getOption('structure')) {

        }

        $output->writeln($text);
    }
}