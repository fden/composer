<?php

namespace Forms;

class Element {

	private $element = '';
	private $label_element = '';
	private $type = '';
	private $label = '';
	private $value = '';
	private $id = '';
	private $option_list = array(1,2,3,4,5,6,7,8,9);
	private $tag = 'undefind';

	private $attr = array(
			'class' => 'form-control'
		);

	private $wrpaAttrs = array(
			'default' => array(
					'class' => 'form-group'
				)
		);



	function __construct($name = '', $array){
		foreach($array as $key => $value){
	    	$this->{$key} = $value;
	    }
	    $this->generateAttr();
		$this->element = $this->{$this->tag}();
		$this->createLabel();
	}

	private function generateAttr(){
		$this->attr['class'] .= ' '.$this->class;		
		$this->attr['id'] = $this->id;		
		if($this->tag == 'select')
			$this->attr['class'] .= ' input-sm'; 
	}

	public function __toString()
    {
    	return $this->wrap();
    }

    public function wrap(){

    	$html = '';
    	$html .= $this->startTag('div', $this->wrpaAttrs['default']);
    	$html .= $this->label.$this->element;
    	$html .= $this->endTag('div');

    	return $html;
    }

	private function createLabel(){
    		$this->label = self::addLabelFor($this->id, $this->label);
    	
	}

	private function undefind(){
		return '';
	}

	private function input() {

    	if(!isset($value)) $value = '';
        $str = "<input type=\"$this->type\" name=\"$this->name\" value=\"$this->value\"";
            $str .= self::addAttributes( $this->attr );
        // $str .= $this->xhtml? ' />': '>';
        $str .= '>';
        return $str;
    }

     function select() {
        $str = "<select name=\"$this->name\"";
            $str .= $this->addAttributes( $this->attr );
        $str .= ">\n";
        if ( isset($this->header) ) {
            $str .= "  <option value=\"\">$header</option>\n";
        }
        foreach ( $this->option_list as $val => $text ) {
            $str .= $val ? "  <option value=\"$val\"": "  <option";
            if ( isset($selected_value) && ( $selected_value === $val || $selected_value === $text) ) {
                $str .= $this->xhtml? ' selected="selected"': ' selected';
            }
            $str .= ">$text</option>\n";
        }
        $str .= "</select>";
        return $str;
    }

    private function startTag($tag, $attr_ar = array() ) {
        $this->tag = $tag;
        $str = "<$tag";
        if ($attr_ar) {
            $str .= $this->addAttributes( $attr_ar );
        }
        $str .= '>';
        return $str;
    }
    
    private function endTag($tag = '') {
        $str = $tag? "</$tag>": "</$this->tag>";
        $this->tag = '';
        return $str;
    }

    private static function addAttributes( $attr_ar ) {
        $str = '';
        // check minimized (boolean) attributes
        $min_atts = array('checked', 'disabled', 'readonly', 'multiple',
                'required', 'autofocus', 'novalidate', 'formnovalidate'); // html5
        foreach( $attr_ar as $key=>$val ) {
            if ( in_array($key, $min_atts) ) {
                if ( !empty($val) ) { 
                    $str .= $this->xhtml? " $key=\"$key\"": " $key";
                }
            } else {
                $str .= " $key=\"$val\"";
            }
        }
        return $str;
    }

    public static function addLabelFor($forID, $text, $attr_ar = array() ) {
        $str = "<label for=\"$forID\"";
        if ($attr_ar) {
            $str .= self::addAttributes( $attr_ar );
        }
        $str .= ">$text</label>";
        return $str;
    }
}