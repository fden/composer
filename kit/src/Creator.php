<?php

// -------------------------COLUMN TYPES

// TARRAY			'array'	
// SIMPLE_ARRAY		'simple_array'	
// JSON_ARRAY		'json_array'	
// BIGINT			'bigint'	
// BOOLEAN			'boolean'	
// DATETIME			'datetime'	
// DATETIMETZ		'datetimetz'	
// DATE				'date'	
// TIME				'time'	
// DECIMAL			'decimal'	
// INTEGER			'integer'	
// OBJECT			'object'	
// SMALLINT			'smallint'	
// STRING			'string'	
// TEXT				'text'	
// BLOB				'blob'	
// FLOAT			'float'	
// GUID				'guid'


// ------------------------COLUMN OPTIONS

// integer|null		length			null
// integer			precision		10
// integer			scale			0
// boolean			unsigned		false
// boolean			fixed			false
// boolean			notnull			true
// string|null		default			null
// boolean			autoincrement	false
// null 			comment 		null
// null 			columnDefinition null


// string							$_name	null	
// Doctrine\DBAL\Schema\Column[]	$_columns	array()	
// Doctrine\DBAL\Schema\Index[]		$_indexes	array()	
// string							$_primaryKeyName	falsHelperse	
// ForeignKeyConstraint[]			$_fkConstraints	array()	
// array							$_options	array()	
// SchemaConfig						$_schemaConfig	null	


use Symfony\Component\Yaml\Yaml;

use \Helpers;
use Helpers\FormHelper;


class Creator
{
	public static $columnOptions = array('length','precision','scale','unsigned','fixed','notnull','default','autoincrement','comment','columnDefinition');
	public static $columnTypes = array('array','simple_array','json_array','bigint','boolean','datetime','datetimetz','date','time','decimal','integer','object','smallint','string','text','blob','float','guid');
	public static $types = array(
		'ai' => array(
			'type' => 'integer',
		    'length' => '11',
		    'autoincrement' => 'true',
		    'index' => 'primary',
		    'unsigned' => 'true'
		)
	);

	function __construct($args = array())
	{
		// new Object();
		// echo \Helpers\foo();
		// Helpers\foo();
		// print_r();die;
		$this->config = Yaml::parse('config/config.yml');
		if($args)
		{
			$task = $args['task'].'Task';
			$this->$task($args); //throw new MyException('task not exist');
		}
	}

	public function dbTask($args)
	{
		if($args['module'])
		{
			// die($this->config['src']."modules/{$args['module']}.yml");
			$array = Yaml::parse($this->config['src']['config']."modules/{$args['module']}.yml");
			// die($array);
		}
		$array = $array['Obj'];
		switch ($args['action']) {
            case 'install':
                Creator::createDb($array);
                break;
            case 'flush':
                Creator::dropDb($array);
                break;
            case 'update':
                Creator::updateDb($array);
                break;
            case 'forms':
                Creator::createForms($array);
                break;
            default:
                $text = 'undefind action';
                break;
        }
	}

	public function formTask($args)
	{
		if($args['module'])
		{
			$array = Yaml::parse($this->config['src']['config']."modules/{$args['module']}.yml");
		}

		$array = $array['Obj'];
		switch ($args['action']) {
            case 'normalize':
                print_r(Object\Object::normalizeObj($array));
                break;
            default:
                $text = 'undefind action';
                break;
        }
	}

	public function objTask($args){
		// if($args['module'])
		// // {
			$array = Yaml::parse($this->config['src']['config']."modules/{$args['module']}.yml");
		// }
			// print_r($array);die;
		$array = $array['Obj'];
		switch ($args['action']) {
            case 'normalize':
                Object\Object::normalizeObjs($array);
                break;
            default:
                $text = 'undefind action';
                break;
        }
	}

	public static function getConfig()
	{
		return Yaml::parse('config/config.yml');
	}

	public static function getDb()
	{
		$driver = new \Doctrine\DBAL\Driver\PDOMySql\Driver;
		return new \Doctrine\DBAL\Connection(self::getConfig()['db'], $driver);
	}

	public static function makeForms()
	{
		new FormHelper();
	}

	public static function createDb($array)
	{
		$schema = self::createDbFromArray($array);
		$sm = self::getDb();
		$platform = $sm->getDatabasePlatform();
		$queries = $schema->toSql($platform);
		// print_r($queries);die;
		foreach ($queries as $key => $value) {
			$sm->query($value);
		}
	}

	public static function updateDb($array)
	{
		$schema = self::createDbFromArray($array);
		$db = self::getDb();
		$sm = $db->getSchemaManager();

		$platform = $db->getDatabasePlatform();
		$queries = $sm->createSchema()->getMigrateToSql($schema, $platform);

		print_r($queries);
	}

	public static function dropDb($array)
	{
		$schema = self::createDbFromArray($array);

		$platform = self::getDb()->getDatabasePlatform();
		$queries = $schema->toDropSql($platform);
		print_r($queries);
	}

	public static function createForms2($array)
	{
		$forms = array();
		foreach ($array as $table => $columns) {
			foreach ($columns as $key => $value) {
				$forms[$table]['add'][$key]['type'] = $value['type'];
				$forms[$table]['view']['filds'][$key] = $value['type'];
			}
		}
		$forms_yaml = Yaml::dump($forms, 5);
		file_put_contents('config/forms.yml', $forms_yaml);
		// print_r($forms);
	}

	public static function renderForms()
	{
		$forms_yaml = Yaml::parse('config/forms.yml');
		// print_r($forms_yaml);
		$formHelper = new FormHelper();
		foreach ($forms_yaml as $key => $value) {
			$formHelper->makeView($key, $value);
		}
	}

	public static function createDbFromArray($tables){

		$schema = new \Doctrine\DBAL\Schema\Schema();
		foreach ($tables as $tableName => $columns) {			
			$table = $schema->createTable($tableName);
			$$tableName = $table;
			foreach ($columns as $columnName => $options) {
				$opt = array();
				$options = self::getDbType($options);
				foreach ($options as $key => $value) {
					if(in_array($key, self::$columnOptions))
						$opt[$key] = $value;
				}

				$table->addColumn($columnName, $options['type'], $opt);
				if(isset($options['fk']))
				{
					list($fkTable, $fkColumn) = explode(',', $options['fk']);
					$table->addForeignKeyConstraint(
						$$fkTable, 
                        array(trim($columnName)), 
                        array(trim($fkColumn)), 
                        array("onDelete" => "CASCADE")
                    );
				}
				if (isset($options['index'])) {
					switch ($options['index']) {
						case 'primary':
							$table->setPrimaryKey(array($columnName));
							break;
						case 'unique':
							$table->addUniqueIndex(array($columnName));
							break;						
						default:
							break;
					}
				}
			}
		}
		return $schema;
	}

	public static function getDbType($opt)
	{
		$type = $opt['type'];
		if(isset(self::$types[$type]) && is_array(self::$types[$type]))
		{
			$opt = array_merge($opt, self::$types[$type]);
		}
		return $opt;
	}

	public static function createForm($array)
	{
		$form = new FormHelper();
		die;
		foreach ($array as $obj => $columns) {
			foreach ($columns as $columnName => $options) {
				if(isset($options['form']))
					print_r($options['form']);
			}
		}
	}
}