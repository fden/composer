<?php

namespace Helpers;


define('Helpers\TYPES', 1);

class Main{}

function foo(){ echo "sdsd";}

$column_types = array(
	'array' 			=> '',
	'simple_array' 		=> '',
	'json_array' 		=> '',
	'bigint' 			=> '',
	'boolean' 			=> '',
	'datetime' 			=> '',
	'datetimetz' 		=> '',
	'date' 				=> '',
	'time' 				=> '',
	'decimal' 			=> '',
	'integer' 			=> '',
	'object' 			=> '',
	'smallint' 			=> '',
	'string' 			=> '',
	'text' 				=> '',
	'blob' 				=> '',
	'float' 			=> '',
	'guid' 				=> '',
	'ai'				=> array(
							'type' => 'integer',
						    'length' => '11',
						    'autoincrement' => 'true',
						    'index' => 'primary',
						    'unsigned' => 'true'
						)
);

$column_options = array(
	'length' 			=> '',
	'precision' 		=> '',
	'scale' 			=> '',
	'unsigned' 			=> '',
	'fixed' 			=> '',
	'notnull' 			=> '',
	'default' 			=> '',
	'autoincrement' 	=> '',
	'comment' 			=> '',
	'columnDefinition' 	=> '',
);

$form_elements = array(
	'text' => '',
	'pass' => '',
	'textarea' => '',
	'select' => '',
	'checkbox' => '',
	'radio' => ''
);

$form_options = array(
	'label' => '',
	'id' => '',
	'class' => '',
	'name' => '',
	'validation' => '',
	'attr' => '',
	'placeholder' => '',
	'wrapper' => '',
);

$type_to_element = array(
	'string' => array(
					'tag'	=> 'input',
					'type'	=> 'text',
				),

	'int'	=>  array(
					'tag'	=> 'input',
					'type'	=> 'text',
					'ui'	=> 'integer',
				),

	'text'	=> array(
					'tag'	=> 'textarea',
					'attr'	=> array(
								'cols' 	=> 30,
								'rows'	=> 4
								),
				),

	'boolean'=> array(
					'tag'	=> 'radio',
					'type'	=> 'yes-no'
				),

	'time'	=> array(
					'tag'	=> 'input',
					'type'	=> 'text',
					'ui'	=> 'date',
				),
);

$optins_to_form = array(
	'notnull'	=> array('req' => 'true'),
	'length'	=> array('validation' => array('max')),
);