<?php

namespace Object;

use Symfony\Component\Yaml\Yaml;
use Helper\FormHelper;

class Object {

	public static $column_types = array(
		'array' 			=> '',
		'simple_array' 		=> '',
		'json_array' 		=> '',
		'bigint' 			=> '',
		'boolean' 			=> '',
		'datetime' 			=> '',
		'datetimetz' 		=> '',
		'date' 				=> '',
		'time' 				=> '',
		'decimal' 			=> '',
		'integer' 			=> '',
		'object' 			=> '',
		'smallint' 			=> '',
		'string' 			=> '',
		'text' 				=> '',
		'blob' 				=> '',
		'float' 			=> '',
		'guid' 				=> '',
		'ai'				=> array(
								'type' => 'integer',
							    'length' => '11',
							    'autoincrement' => 'true',
							    'index' => 'primary',
							    'unsigned' => 'true'
							)
	);

	public static $column_options = array(
		'length' 			=> '',
		'precision' 		=> '',
		'scale' 			=> '',
		'unsigned' 			=> '',
		'fixed' 			=> '',
		'notnull' 			=> '',
		'default' 			=> '',
		'autoincrement' 	=> '',
		'comment' 			=> '',
		'columnDefinition' 	=> '',
	);

	public static $form_elements = array(
		'text' => '',
		'pass' => '',
		'textarea' => '',
		'select' => '',
		'checkbox' => '',
		'radio' => ''
	);

	public static $form_options = array(
		'label' => '',
		'id' => '',
		'class' => '',
		'name' => '',
		'validation' => '',
		'attr' => '',
		'placeholder' => '',
		'wrapper' => '',
	);

	public static $type_to_element = array(
		'string' => array(
						'tag'	=> 'input',
						'type'	=> 'text',
					),

		'integer'	=>  array(
						'tag'	=> 'input',
						'type'	=> 'text',
						'ui'	=> 'integer',
					),

		'text'	=> array(
						'tag'	=> 'textarea',
						'attr'	=> array(
									'cols' 	=> 30,
									'rows'	=> 4
									),
					),

		'boolean'=> array(
						'tag'	=> 'radio',
						'type'	=> 'yes-no'
					),

		'time'	=> array(
						'tag'	=> 'input',
						'type'	=> 'text',
						'ui'	=> 'date',
					),
	);

	public static $optins_to_form = array(

		'notnull'	=> array('req' => 'true'),
		'length'	=> array('validation' => array('max')),
	);

	public $form = array();


	// --- CONSTRUCT-----
	function __construct($config_dir = '../src/config/'){

		// $this->obj = Yaml::parse($config_dir.'modules/media.yml');
		// $this->obj = self::normalizeObjs($this->obj['Obj']);
		$this->obj = self::getObject($config_dir);
	}

	public static function getObject($config_dir){
		// echo "<pre>";
		// print_r(json_decode(file_get_contents($config_dir.'obj.json'), true));die;
		return json_decode(file_get_contents($config_dir.'obj.json'), true);
	}

	public static function normalizeObjs($obj){
		$result = array();
		foreach ($obj as $key => $value) {
			$normalized = self::normalizeObj($value, $key);
			$result['forms'][$key] = $normalized['form'];
			unset($normalized['form']);
			$result['db'][$key] = $normalized;
		}
		file_put_contents('../src/config/obj.json', json_encode($result));
	}

	public static function normalizeObj($obj, $name){

		$result = array('forms' => array());
		$forms = array();
		foreach ($obj as $column => $options) {
			
			$type = $options['type'];
			unset($options['type']);
			$result[$column] = array_merge(self::normalizeDbTypes($type), $options);
			if(isset($options['form'])){
				if(!is_array($options['form']))
					$options['form'] = array();
				// $result['form'][$column] = array_merge($options['form'], self::normalizeFormElements($result[$column], $column));
				$normalized = self::normalizeFormElements($result[$column], $column);
				$result['form'][$column] = array_merge($options['form'], $normalized);
				// print_r($result['form'][$column]);
				// $forms[$column] = \Helper\FormHelper::element($result[$column]['form']);
				$forms[$column] = $result[$column]['form'];
			}
		}
		// $result['form'] = $forms;
		return $result;
	}

	private static function createForm(){
		
	}

	public static function normalizeDbTypes($type){
		return self::$column_types[$type] ? self::$column_types[$type] : array('type' => $type);
	}

	public static function normalizeFormElements($obj, $name){

		$form = self::$type_to_element[$obj['type']] ? self::$type_to_element[$obj['type']] : array();
		if(isset($obj['form']['tag']))
			unset($form['tag']);
		$form['id'] = $name;
		$form['class'] = $name; 
		$form['name'] = $name; 
		$form['label'] = $name; 

		return $form;
	}

	public function getForm(){
		// $form = new FormHelper();
		$str = '';
		foreach ($this->obj['form'] as $key => $value) {
			$str .= $value;
		}
		return $str;
	}

}