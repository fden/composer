<?php
$p = array();

$p['panel'] = <<<EOF
<div class="block-top-header">
  <h1><img src="images/mod-sml.png" alt="" />{{ title }}</h1>
  <div class="divider"><span></span></div>
</div>
<p class="info">Here you can manage your activity showcase items.</p>
<div class="block-border">
  <div class="block-header">
  	functions
  </div>
  <div class="block-content">
    <div class="utility">
      <table class="display">
        <tr>
          <td>
          Filters
          </td>
          <td class="right"></td>
        </tr>
      </table>
    </div>
    <table class="display sortable-table">
      <thead>
        <tr>
          <th class="firstrow">#</th>
          <th class="left sortable">Name</th>
          <th class="left sortable">Category</th>
          <th>edit</th>
          <th>delete</th>
        </tr>
      </thead>
      <tfoot>
        <tr>
          <td colspan="5"><div class="pagination">1 2 3</div></td>
        </tr>
      </tfoot>
      <tbody>
        <?php if(false):?>
        <tr>
          <td colspan="5"></td>
        </tr>
        <?php else:?>
        <?php ?>
        <tr>
          <th>1</th>
          <td>title</td>
          <td>category</td>
          <td class="center"><a>edit</a></td>
          <td class="center"><a>delete</a></td>
        </tr>
        <?php ?>
        <?php endif;?>
      </tbody>
    </table>
  </div>
</div>
EOF;

echo $p['panel'];