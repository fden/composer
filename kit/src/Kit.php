<?php

use Object\Object;

class Kit
{
	public $obj = null;
	function __construct($config_dir = '../src/config/'){

		$this->obj = self::getObj($config_dir);
	}

	public static function getObj($config_dir){
		
		return new Object($config_dir);
	}

	public function createForm($name){
		return new Forms\Form($this->obj->obj['forms'][$name]);
	}

}